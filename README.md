Simple module to change the image of the pause icon shown when the game is paused, to make your UI just that little more unique.

Change the image using a picker, set Image size, rotation speed and even the pause message from the module configuration screen.

And enjouy a simple UI change!

Spanish localization added by user: Jose Lozano. Thanks!!